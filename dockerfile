FROM debian

RUN apt-get update && \
    apt-get install -y \
        php5-cli \
        php5-imagick \
        php5-intl \
        php5-mcrypt \
        php5-mysql \
        php-apc && \
        git && \
    rm -rf /var/lib/apt/lists/*

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin && \
    ln -s /usr/local/bin/composer.phar /usr/local/bin/composer

RUN git clone https://bitbucket.org/novebeta/niagahoster.git app

RUN composer update -d app/

COPY app/ /app

ENTRYPOINT ["php", "-S 0.0.0.0:8000 -t app/web"]
