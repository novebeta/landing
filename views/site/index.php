<?php

/* @var $this yii\web\View */

$this->title = 'Niagahoster';
?>
<div class="hosting-feature-banner-container-default reset-box-sizing" style="border-bottom: 1px solid #f0f0f0 !important">
    <div class="container feature-banner-wrapper">
        <div class="mar_top5 hidden768"></div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-xxs-12 set768 set640">
            <div class="vertical-middle-child">
                <h1 class="heading-headline heading-headline-big no-margin font-m-bold">PHP Hosting</h1>
                <h3 class="no-transform no-margin font-m-light sub-headline">Cepat, handal, penuh dengan modul PHP yang Anda butuhkan</h3>
                <div class="mar_top1 hidden768"></div>
                <ul class="font-m-light list-check-container">
                    <li>Solusi PHP untuk performa query yang lebih cepat</li>
                    <li>Konsumsi memori yang lebih rendah</li>
                    <li>Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6, PHP 7</li>
                    <li>Fitur enkripsi ioncube dan Zend Guard Loader</li>
                </ul>
            </div>
            <div class="mar_top1"></div>
        </div>
        <div class="col-lg-7 col-md-7 col-xs-6 hidden-sm hidden-xs text-right">
            <div class="mar_top10 hidden-lg hidden-xs"></div>
            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/php_hosting.png"  style="width:500px;height:250px;">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="hosting-feature-banner-container-default reset-box-sizing">
    <div class="container">
        <div class="mar_top5 hidden768"></div>
        <div class="col-lg-2 col-md-2 col-xs-2"></div>
        <div class="col-lg-3 col-md-3 col-xs-3 hidden-sm hidden-xs text-center">
            <div style="height: 175px;display: flex;  align-items: center;  justify-content: center">
                <img class="box" src="<?php echo Yii::$app->request->baseUrl;?>/images/zendguard.png">
            </div>

            PHP Zend Guard Loader
        </div>
        <div class="col-lg-2 col-md-2 col-xs-2 hidden-sm hidden-xs text-center">
            <div class="align-middle" style="height: 175px;display: flex;  align-items: center;  justify-content: center"><img src="<?php echo Yii::$app->request->baseUrl;?>/images/composer.png"></div>
            PHP Composer
        </div>
        <div class="col-lg-3 col-md-3 col-xs-3 hidden-sm hidden-xs text-center">
            <div class="align-middle" style="height: 175px;display: flex;  align-items: center;  justify-content: center">
                <img src="<?php echo Yii::$app->request->baseUrl;?>/images/ioncube.png">
            </div>
            PHP IonCube Loader
        </div>
        <div class="col-lg-2 col-md-2 col-xs-2"></div>
    </div>
    <br>
    <br>
</div>
<style>
    h2 {font-size: 30px;}
</style>
<div class="hosting-package-container reset-box-sizing">
    <div class="mar_top5"></div>
    <div class="container">
        <div class="row">
            <h2 class="font-m-bold text-center hidden-xxs no-transform">Paket Hosting Singapura yang Tepat</h2>
            <p class="font-m-light text-center no-margin hidden-xxs no-transform size-h2">Diskon 40% + Domain dan SSL Gratis untuk Anda</p>
            <div class="mar_top1"></div>
        </div>
        <style>
            .hosting-price-wrapper .fa-star {color: #298eea; }
            .hosting-price-column.border-blue {border:1px solid #298eea !important;}
        </style>
        <div class="container-wrapper ab-control">
            <div class="hosting-price-table">
                <div class="hosting-price-table-wrapper">
                    <div class="hosting-price-wrapper">
                        <div class="hosting-price-column">
                            <div class="title"> Bayi </div>
                            <div class="price">
                                <div id="bayi-old" class="old-price strike-black"></div>
                                <div id="bayi-new" class="new-price"></div>
                            </div>
                            <div id="bayi-user" class="resource font-m-light">
                            </div>
                            <div class="content-wrapper">
                                <ul class="font-r-normal">
                                    <li><b>500 MB</b> Disk Space </li>
                                    <li><b>Unlimited</b> Bandwidth </li>
                                    <li><b>Unlimited</b> Databases </li>
                                    <li><b>1</b> Domain </li>
                                    <li><b>Instant</b> Backup </li>
                                    <li><b>Unlimited SSL</b> Gratis Selamanya </li>
                                </ul>
                            </div>
                            <div class="button-depth-container">
                                <a href="" class="button-depth-new button-depth-domain-home button-depth-3d-new btn-white-black gtmProductView cta-control" data-product="Bayi" data-label="Section Table Price">
                                    Pilih Sekarang </a>
                            </div>
                        </div>
                    </div>
                    <div class="hosting-price-wrapper">
                        <div class="hosting-price-column">
                            <div class="title"> Pelajar </div>
                            <div class="price">
                                <div id="pelajar-old" class="old-price strike-black"></div>
                                <div id="pelajar-new" class="new-price"></div>
                            </div>
                            <div id="pelajar-user" class="resource font-m-light">
                            </div>
                            <div class="content-wrapper">
                                <ul class="font-r-normal">
                                    <li><b>Unlimited</b> Disk Space </li>
                                    <li><b>Unlimited</b> Bandwidth </li>
                                    <li><b>Unlimited</b> POP3 Email </li>
                                    <li><b>Unlimited</b> Databases </li>
                                    <li><b>10</b> Addon Domains </li>
                                    <li><b>Instant</b> Backup </li>
                                    <li><b>Domain Gratis</b> Selamanya </li>
                                    <li><b>Unlimited SSL</b> Gratis Selamanya </li>
                                </ul>
                            </div>
                            <div class="button-depth-container">
                                <a href="" class="button-depth-new button-depth-domain-home button-depth-3d-new btn-white-black gtmProductView cta-control" data-product="Pelajar" data-label="Section Table Price">
                                    Pilih Sekarang </a>
                            </div>
                        </div>
                    </div>
                    <div class="hosting-price-wrapper hosting-price-highlight">
                        <div class="hosting-price-column border-blue">
                            <div class="title" style="background-color:#298eea!important;color:#fff !important;border:none !important;">Personal</div>
                            <div class="price" style="background-color:#298eea;border:none !important;">
                                <div id="personal-old" class="old-price strike-white" style="color:#fff !important;"></div>
                                <div id="personal-new" class="new-price" style="color:#fff !important;"></div>
                            </div>
                            <div id="personal-user"  class="resource font-m-light" style="background-color:#177eda;color:#fff !important;">
                            </div>
                            <div class="content-wrapper">
                                <ul class="font-r-normal">
                                    <li><b>Unlimited</b> Disk Space</li>
                                    <li><b>Unlimited</b> Bandwidth</li>
                                    <li><b>Unlimited</b> POP3 Email</li>
                                    <li><b>Unlimited</b> Databases</li>
                                    <li><b>Unlimited</b> Addon Domains</li>
                                    <li><b>Instant</b> Backup</li>
                                    <li><b>Domain Gratis</b> Selamanya</li>
                                    <li><b>Unlimited SSL</b> Gratis Selamanya</li>
                                    <li><b>SpamAssasin</b> Mail Protection</li>
                                </ul>
                            </div>
                            <div class="button-depth-container"> <a href="" class="button-depth-new button-depth-domain-home button-depth-3d-new btn-blue-white gtmProductView cta-control" data-product="Personal" data-label="Section Table Price">Pilih Sekarang</a> </div>
                        </div>
                    </div>
                    <div class="hosting-price-wrapper">
                        <div class="hosting-price-column">
                            <div class="title">Bisnis</div>
                            <div class="price">
                                <div id="bisnis-old"  class="old-price strike-black"></div>
                                <div id="bisnis-new" class="new-price"></div>
                            </div>
                            <div id="bisnis-user"  class="resource font-m-light">
                            </div>
                            <div class="content-wrapper">
                                <ul class="font-r-normal">
                                    <li><b>Unlimited</b> Disk Space</li>
                                    <li><b>Unlimited</b> Bandwidth</li>
                                    <li><b>Unlimited</b> POP3 Email</li>
                                    <li><b>Unlimited</b> Databases</li>
                                    <li><b>Unlimited</b> Addon Domains</li>
                                    <li><b>Magic</b> Auto Backup &amp; Restore</li>
                                    <li><b>Domain Gratis</b> Selamanya</li>
                                    <li><b>Unlimited SSL</b> Gratis Selamanya</li>
                                    <li><b>Prioritas</b> Layanan Support</li>
                                    <li><b><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <br>SpamExpert Pro</b> Mail Protection</li>
                                </ul>
                            </div>
                            <div class="button-depth-container">
                                <a href="" class="button-depth-new button-depth-domain-home button-depth-3d-new btn-white-black gtmProductView cta-control" data-product="Bisnis" data-label="Section Table Price">
                                    Pilih Sekarang </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    ul.striped-list li:before {
        font-family: FontAwesome;
        content: '\f058';
        margin: 0 5px 0 -15px;
        color: #39b151
    }

    ul.striped-list {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-width: 500px;
    }
    ul.striped-list > li {
        padding-left: 25px!important;
        line-height: 20px;
        border: 1px solid #f0f0f0!important;
        padding: 6px;
    }
    ul.striped-list > li:nth-of-type(even) {
        background-color: #f7f7f7 ;
    }
    ul.striped-list > li:last-child {
        border-bottom: none;
    }

    ul.striped-list > li >span{
        display:inline-block;
        width:300px;
        text-align: center;
    }
</style>
<div class="hosting-feature-banner-container-default reset-box-sizing">
    <div class="container">
        <div class="mar_top5 hidden768"></div>
        <p class="font-m-light text-center no-margin hidden-xxs no-transform size-h2">Powerful dengan Limit PHP yang Lebih Besar</p>
        <div class="mar_top5 hidden768"></div>
        <div class="col-lg-2 col-md-2 col-sm-hidden col-xs-hidden col-xxs-hidden set768 set640"></div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-xxs-12 set768 set640">
            <ul class="font-m-light striped-list ">
                <li ><span >max execution time 300s</span></li>
                <li ><span >max execution time 300s</span></li>
                <li ><span >php memory limit 1024 MB</span></li>
            </ul>
            <div class="mar_top1"></div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-xxs-12 set768 set640">
            <div class="vertical-middle-child">
                <div class="mar_top1 hidden768"></div>
                <ul class="font-m-light striped-list">
                    <li><span >post max size 128 MB</span></li>
                    <li><span >upload max filesize 128 MB</span></li>
                    <li><span >max input vars 2500</span></li>
                </ul>
            </div>
            <div class="mar_top1"></div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-hidden col-xs-hidden col-xxs-hidden set768 set640"></div>
    </div>
    <br>
    <br>
</div>
<div class="clearfix"></div>
<div class="container">
    <p class="font-m-light text-center no-margin hidden-xxs no-transform size-h2">Semua Paket Hosting Sudah Termasuk</p>
    <div class="mar_top5 hidden768"></div>
    <div class="col-lg-4 col-md-4 col-xs-4 hidden-sm hidden-xs text-center">
        <div style="height: 125px;display: flex;  align-items: center;  justify-content: center">
            <img class="box" src="<?php echo Yii::$app->request->baseUrl;?>/images/php.svg" width="64px" height="64px">
        </div>
        <p style="font-size: larger;font-weight: bold;margin:0;margin:0;">PHP Semua Versi</p>
        <p>Pilih mulai dari versi PHP 5.3 s/d PHP 7<br>
            Ubah sesuka Anda!</p>
    </div>
    <div class="col-lg-4 col-md-4 col-xs-4 hidden-sm hidden-xs text-center">
        <div class="align-middle" style="height: 125px;display: flex;  align-items: center;  justify-content: center">
            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/mysql.svg" width="64px" height="64px"></div>
        <p style="font-size: larger;font-weight: bold;margin:0;">MySQL Versi 5.6</p>
        <p>Nikmati MySQL versi terbaru, tercepat dan<br>
            kaya akan fitur.</p>
    </div>
    <div class="col-lg-4 col-md-4 col-xs-4 hidden-sm hidden-xs text-center">
        <div class="align-middle" style="height: 125px;display: flex;  align-items: center;  justify-content: center">
            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/cpanel.svg" width="64px" height="64px">
        </div>
        <p style="font-size: larger;font-weight: bold;margin:0;">Panel Hosting cPanel</p>
        <p> Kelola website dengan panel canggih yang<br>
            familiar di hati Anda.</p>
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12 hidden-sm hidden-xs text-center">
        <div class="col-lg-4 col-md-4 col-xs-4 hidden-sm hidden-xs text-center">
            <div style="height: 125px;display: flex;  align-items: center;  justify-content: center">
                <img class="box" src="<?php echo Yii::$app->request->baseUrl;?>/images/garansi.svg" width="64px" height="64px">
            </div>
            <p style="font-size: larger;font-weight: bold;margin:0;">Garansi Uptime 99.9%</p>
            <p>Data center yang mendukung kelangsungan<br>
                website Anda 24/7.</p>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-4 hidden-sm hidden-xs text-center">
            <div class="align-middle" style="height: 125px;display: flex;  align-items: center;  justify-content: center">
                <img src="<?php echo Yii::$app->request->baseUrl;?>/images/innodb.svg" width="64px" height="64px" ></div>
            <p style="font-size: larger;font-weight: bold;margin:0;">Database InnoDB Unlimited</p>
            <p>Jumlah dan ukuran database yang tumbuh<br>
                sesuai kebutuhan Anda.</p>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-4 hidden-sm hidden-xs text-center">
            <div class="align-middle" style="height: 125px;display: flex;  align-items: center;  justify-content: center">
                <img src="<?php echo Yii::$app->request->baseUrl;?>/images/remote.svg" width="64px" height="64px">
            </div>
            <p style="font-size: larger;font-weight: bold;margin:0;">Wildcard Remote MySQL</p>
            <p>Mendukung s/d 25 max_user_connections<br>
                dan 100 max_connections.</p>
        </div>
    </div>
    <br>
    <br>
    <br>
</div>
<div class="clearfix"></div>
<div class="hosting-feature-banner-container-default reset-box-sizing" style="border-bottom: 1px solid #f0f0f0 !important">
    <div class="container">
        <div class="mar_top5 hidden768"></div>
        <p class="font-m-light text-center no-margin hidden-xxs no-transform size-h2">Mendukung Penuh Framework Laravel</p>
        <div class="mar_top5 hidden768"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-xxs-12 set768 set640">
            <div class="vertical-middle-child">
                <div class="mar_top1 hidden768"></div>
                <p >
                    Tak perlu menggunakan dedicated server ataupun vps
                    yang mahal. Layanan PHP hosting murah kami
                    mendukung penuh framework favorit Anda.
                </p>
                <ul class="font-m-light list-check-container">
                    <li>Install Laravel <b>1 klik</b> dengan softaculous Installer</li>
                    <li>Mendukung ekstensi <b>PHP MCrypt, phar, mbstring, json, </b>dan <b>fileinfo</b></li>
                    <li>Tersedia <b>Composer</b> dan <b>SSH</b> untuk menginstall packages pilihan Anda.</li>
                </ul>
                <p class="font-s-light" style="font-size: smaller">Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis</p>
                <a class="button-depth-new button-depth-domain-home button-depth-3d-new btn-blue-white gtmProductView" >Pilih Hosting Anda</a>
            </div>
            <div class="mar_top1"></div>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-6 hidden-sm hidden-xs text-right">
            <div class="mar_top10 hidden-lg hidden-xs"></div>
            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/php_laravel.png"  style="width:470px;height:300px;">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="hosting-feature-banner-container-default reset-box-sizing">
    <div class="container">
        <div class="mar_top5 hidden768"></div>
        <p class="font-m-light text-center no-margin hidden-xxs no-transform size-h2">Modul Lengkap untuk Menjalankan Aplikasi PHP Anda</p>
        <div class="mar_top5 hidden768"></div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-6 set768 set640">
            <p>
                IcePHP<br>
                apc<br>
                apcu<br>
                apm<br>
                ares<br>
                bcmath<br>
                bcompiler<br>
                big_int<br>
                bigset<br>
                bloomy<br>
                bz2_filter<br>
                clamav<br>
                coin_acceptor<br>
                crack<br>
                dba<br>
            </p>
            <div class="mar_top1"></div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-6 set768 set640">
            <p>
                http<br>
                huffman<br>
                idn<br>
                igbinary<br>
                imagick<br>
                imap<br>
                inclued<br>
                inotify<br>
                interbase<br>
                intl<br>
                ioncube_loader<br>
                ioncube_loader_4<br>
                jsmin<br>
                json<br>
                ldap<br>
            </p>
            <div class="mar_top1"></div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-6 set768 set640">
            <p>
                nd_pdo_mysql<br>
                oauth<br>
                oci8<br>
                odbc<br>
                opcache<br>
                pdf<br>
                pdo<br>
                pdo_dblib<br>
                pdo_firebird<br>
                pdo_mysql<br>
                pdo_odbc<br>
                pdo_pgsql<br>
                pdo_sqlite<br>
                pgsql<br>
                phalcon<br>
            </p>
            <div class="mar_top1"></div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-6 set768 set640">
            <p>
                stats<br>
                stem<br>
                stomp<br>
                suhosin<br>
                sybase_ct<br>
                sysvmsg<br>
                sysvsem<br>
                sysvshm<br>
                tidy<br>
                timezonedb<br>
                trader<br>
                translit<br>
                uploadprogress<br>
                uri_template<br>
                uuid<br>
            </p>
            <div class="mar_top1"></div>
        </div>
        <div class="text-center">
            <a class="button-depth-new button-depth-domain-home button-depth-3d-new btn-white-black gtmProductView" >Selengkapnya</a>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="hosting-feature-banner-container-default reset-box-sizing" >
    <div class="container " style="padding-bottom: 0px">
        <div class="mar_top5 hidden768"></div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-xxs-12 ">
            <div class="vertical-middle-child">
                <h3 class="no-transform no-margin font-m-light sub-headline">Linux Hosting yang Stabil dengan Teknologi Live</h3>
                <div class="mar_top1 hidden768"></div>
                <p >
                    SuperMicro <b>Intel Xeon 24-Cores</b> server dengan RAM <b>128GB</b> dan teknologi <b>LVE CloudLinux</b> untuk stabilitas server Anda. Dilengkapi
                    dengan <b>SSD</b> untuk kecepatan <b>Mysql</b> dan caching, Apache load balancer berbasis LiteSpeed Technologies, <b>CageFS</b> security, <b>Raid-10</b> protection
                    dan auto backup untuk keamanan website PHP Anda.
                </p>
                <a class="button-depth-new button-depth-domain-home button-depth-3d-new btn-blue-white gtmProductView" >Pilih Hosting Anda</a>
            </div>
            <div class="mar_top1"></div>
        </div>
        <div class="col-lg-7 col-md-7 col-xs-6 hidden-sm hidden-xs text-right">
            <div class="mar_top10 hidden-lg hidden-xs"></div>
            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/image_support.png"  style="width:550px;">
        </div>
    </div>
</div>
<?php
$this->registerJs(<<<JS

    var harga = '{"hosting":{"bayi":{"old":"27.800","new":"19.800","user":"2.031"},"pelajar":{"old":"64.800","new":"44.800","user":"6.583"},"personal":{"old":"104.250","new":"67.800","user":"16.228"},"bisnis":{"old":"153.800","new":"91.800","user":"6.030"}}}';
    
    $( document ).ready(function() {
        let data = JSON.parse(harga);
        $("#bayi-old").html("<sup class=\"rp\">Rp</sup> "+data.hosting.bayi.old);
        let bayi_new = data.hosting.bayi.new.split(".");
        $("#bayi-new").html("<sup><span>Rp</span></sup> <span>"+bayi_new[0]+"</span><sup>."+bayi_new[1]+"/ <span>bln</span></sup>");
        $("#bayi-user").html("<b>"+data.hosting.bayi.user+"</b> Pengguna Terdaftar");
        
        $("#pelajar-old").html("<sup class=\"rp\">Rp</sup> "+data.hosting.pelajar.old);
        let pelajar_new = data.hosting.pelajar.new.split(".");
        $("#pelajar-new").html("<sup><span>Rp</span></sup> <span>"+pelajar_new[0]+"</span><sup>."+pelajar_new[1]+"/ <span>bln</span></sup>");
        $("#pelajar-user").html("<b>"+data.hosting.pelajar.user+"</b> Pengguna Terdaftar");
        
        $("#personal-old").html("<sup class=\"rp\">Rp</sup> "+data.hosting.personal.old);
        let personal_new = data.hosting.personal.new.split(".");
        $("#personal-new").html("<sup><span>Rp</span></sup> <span>"+personal_new[0]+"</span><sup>."+personal_new[1]+"/ <span>bln</span></sup>");
        $("#personal-user").html("<b>"+data.hosting.personal.user+"</b> Pengguna Terdaftar");
        
        $("#bisnis-old").html("<sup class=\"rp\">Rp</sup> "+data.hosting.bisnis.old);
        let bisnis_new = data.hosting.bisnis.new.split(".");
        $("#bisnis-new").html("<sup><span>Rp</span></sup> <span>"+bisnis_new[0]+"</span><sup>."+bisnis_new[1]+"/ <span>bln</span></sup>");
        $("#bisnis-user").html("<b>"+data.hosting.bisnis.user+"</b> Pengguna Terdaftar");
    });
JS
);