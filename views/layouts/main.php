<?php

/* @var $this View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\web\View;

AppAsset::register($this);
rmrevin\yii\fontawesome\AssetBundle::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
        <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
        <script>
            (function ($) {
                $("#btn-share").jsSocials({
                    url: "https://www.niagahoster.co.id",
                    text: "Hosting Murah Indonesia Unlimited - Niagahoster",
                    showCount: true,
                    showLabel: false,
                    shareIn: "popup",
                    shares: [
                        { share: "twitter", via: "niagahoster", hashtags: "" },
                        "facebook",
                        "googleplus",
                    ]
                });
            }(jQuery));
        </script>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="site_wrapper">

        <div class="c_full">
            <div class="top_links hidden768 hidden800">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-sm" style="padding-left:0px;">
                        <div class="ebook-promo hidden-xxs">
                            <img class="deferImg" src="<?php echo Yii::$app->request->baseUrl;?>/images/ribbon.png">
                            <p class="font-r-light">
                                <a href="" target="_blank" data-mce-href="">Gratis Ebook 9 Cara Cerdas Menggunakan Domain</a>
                                <span id="close-promo">[ x ]</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-sm" style="padding-right:0px">
                        <div class="contact_info nav-menu-icon clearfix hidden-xxs hidden-xss">
                            <ul class="clearfix fa-ul">
                                <li> <a href="tel:02742920443" class="text-white call-phone font-r-light">0274-2920443 </a> </li>
                                <li> <a class="livechatlink call-live font-r-light">Live Chat</a> </li>
                                <li> <a class="text-white call-login font-r-light" id="menu_login_open">Member Area </a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="c_full">
            <div class="top_section height-auto">
                <div class="container">
                    <div id="logo" class="hidden-md hidden-sm hidden-xs hidden-xxs">
                        <a class="site_logo"> <img src="<?php echo Yii::$app->request->baseUrl;?>/images/logo2.png" class="logo" alt="hosting murah niagahoster"> </a>
                    </div>
                    <nav class="navigation-container float-right clearfix menuiu hidden-md hidden-sm hidden-xs hidden-xxs" role="navigation">
                        <div class="navigation-wrapper">
                            <ul id="main-tiny-menu" class="show-fitur main-navigation float-left clearfix">
                                <li> <a >Hosting</a></li>
                                <li> <a >Domain</a> </li>
                                <li><a >Server</a></li>
                                <li> <a >Website</a> </li>
                                <li> <a >Afiliasi</a> </li>
                                <li> <a >Promo</a> </li>
                                <li><a >Pembayaran</a> </li>
                                <li> <a >Review</a> </li>
                                <li> <a >Kontak</a> </li>
                                <li> <a >Blog</a> </li>
                            </ul>
                        </div>
                    </nav>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <?php echo $content; ?>

        <style>
            .w3_whatsapp_btn {background-image: url("images/icon-wa.png"); border: 1px solid rgba(0, 0, 0, 0.1); display: inline-block !important; position: relative; font-family: Arial, sans-serif; letter-spacing: .4px; cursor: pointer; font-weight: 400; text-transform: none; color: #fff; border-radius: 2px; background-color: #5cbe4a; background-repeat: no-repeat; line-height: 1.2; text-decoration: none; text-align: left; }
            .w3_whatsapp_btn_small {font-size: 12px; background-size: 16px; background-position: 5px 2px; padding: 3px 6px 3px 25px; }
            a.whatsapp {color: #fff; }
            .fb-like span {vertical-align: top !important; margin-top: -4px; }

            @media only screen and (min-width:981px) {.fb_iframe_widget {display: inline-block; float: left; margin-right: 25px; position: relative; } }
        </style>
        <div class="support-banner-container reset-box-sizing defer-load" style="background: rgb(247, 247, 247); display: block;">
            <div class="container container-wrapper">
                <div class="mar_top2 hidden-xxs"></div>
                <div class="col-md-6 col-sm-7 no-padding col-xs-7 col-xxs-12 sosmed-text-640">
                    <h5 class="font-m-light no-padding no-float text-white no-transform hidden-xs hidden-xxs" style="color:#000000 !important;font-size:15px !important;line-height:8px !important;">Bagikan jika Anda menyukai halaman ini.</h5>
                </div>
                <div class="col-md-6 col-sm-5 col-xs-5 no-padding col-xxs-12 sosmed-text-640" style="margin-top:-11px;">
                    <div class="other-sos-med">
                        <div class="col-lg-12 col-md-12">
                            <div id="btn-share" style="margin-top:6px;" class="jssocials">
                            </div>
                        </div>
                    </div>
                    <div class="mar_top1"></div>
                </div>
            </div>
            <style>
                .support-banner-container .font-m-bold,.support-banner-container .font-m-light{color:#fff!important;font-size:32px!important}
                .support-banner-container .zmdi.pull-left{margin-right:20px!important}
                .support-banner-container .button-depth-3d-new{border:2px solid #fff!important;color:#fff!important;padding:6px 25px!important}
                .support-banner-container .button-depth-3d-new:hover{background-color:#fff!important;color:#22a1f0!important}
                .support-banner-container .button-depth-icon:{padding-left:0!important}
                @media screen and (min-width:320px) and (max-width:479px){.support-banner-container .zmdi.pull-left{margin:0!important}
                    .support-banner-container .button-depth-3d-new{padding:6px 35px!important}
                    .support-banner-container .button-depth-new{margin-left:55px}
                }
                @media screen and (min-width:480px) and (max-width:760px){.support-banner-container .button-depth-new{margin-left:115px}
                }
            </style>
            <div class="support-banner-container reset-box-sizing defer-load" style="display: block;">
                <div class="mar_top5 hidden-md hidden-xs hidden-xxs"></div>
                <div class="mar_top2 hidden-lg hidden-sm show-xs show-xxs"></div>
                <div class="container container-wrapper">
                    <div class="col-md-9 col-ms-9 col-sm-12 no-padding col-xxs-12 support-banner-desc">
                        <div class="mar_top1"></div>
                        <h2 class="no-transform no-padding center-block no-float text-white hidden-sm hidden-xs hidden-xxs">
                            <span class="font-m-light text-white">Perlu</span> <span class="font-m-bold text-white">BANTUAN?</span>
                            <span class="font-m-light text-white">Hubungi Kami: </span>
                            <span class="font-m-bold text-white"><a href="" title="0274-2920443">0274 - 2920443</a></span>
                        </h2>
                        <div class="mar_top2"></div>
                    </div>
                    <div class="col-md-3 col-ms-3 col-sm-4 col-xs-4 left-border-super-soft col-xxs-12 center-text hidden-md hidden-sm hidden-xs hidden-xxs support-banner-desc" style="padding-top:0px !important;">
                        <div class="float-right float-no">
                            <div class="button-depth-container">
                                <a href=""  class="button-depth-new button-depth-3d-new button-depth-icon bg-blue text-white pull-right button-icon-chat livechatlink" style="display: table;">
                                    <i class="zmdi zmdi-comments zmdi-hc-2x pull-left"></i> Live Chat
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 no-padding col-xxs-12 center-block hidden-lg show-sm show-xs show-xxs support-banner-desc-mobile">
                        <div class="button-depth-container">
                            <a href=""  class="button-depth-new button-depth-3d-new button-depth-icon bg-blue text-white pull-left button-icon-chat livechatlink" style="display: block; font-size: 18px !important;">
                                <i class="zmdi zmdi-comments zmdi-hc-2x pull-left" style="padding-right:13px;"></i> Live Chat
                            </a>
                        </div>
                    </div>
                    <div class="clearfix mar_top3 hidden-md"></div>
                    <div class="clearfix mar_top2 hidden-lg hidden-sm hidden-xs hidden-xxs"></div>
                </div>
            </div>
            <style>
                .footer .font-r-bold.title{font-size:14px!important;color:#8d8d8d!important}
                .footer .font-r-bold.text,.footer .font-r-normal.text{font-size:14px!important;color:#d7d7d7!important}
                .footer .font-r-light.note{font-size:14px!important;color:#777!important}
                .footer hr.super-soft{border-bottom:1px solid #333!important;border-top:2px solid #333!important}
                .footer .social-icon{color:#d7d7d7!important;position:inherit}
                .footer .footer-newsletter{margin-left:60px}
                .copyright_info hr.super-soft{border-bottom:1px solid #2d2d2d!important;border-top:1px solid #2d2d2d!important}
                .zmdi-hc-border-circle{border:.1em solid #d7d7d7!important;padding:.4em .5em!important}
                .zmdi-hc-border-circle-fb{border:.1em solid #d7d7d7!important;border-radius:50%;padding:.6em .8em!important}
                .zmdi-hc-border-circle-tw{border:.1em solid #d7d7d7!important;border-radius:50%;padding:.6em!important}
                .fa-border{border:.1em solid #d7d7d7!important;border-radius:50%;padding:.6em .4em!important}
                ul.logo-bank li{width:7%;float:left;margin-right:10px}
                @media screen and (min-width:360px) and (max-width:480px){.btn2{padding:0 10px!important;margin:0!important}
                    .footer .footer-menu,.footer .footer-newsletter,.footer .footer-pembayaran,.footer .footer-tutorial{margin-left:0}
                }
                @media only screen and (min-width:320px) and (max-width:639px){.col-xxs-12{width:100%!important}
                }
            </style>
            <div class="footer defer-load" style="display: block;">
                <div class="footer_center">
                    <div class="container center-text hidden-md hidden1024 hidden640 hidden768">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 hidden640 info-callme">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <p class="font-r-bold title">HUBUNGI KAMI</p>
                                <div class="any_questions">
                                    <div class="row">
                                        <div class="mar_top1 hidden-xs hidden-xxs"></div>
                                        <div class="mar_top2 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                        <div class="col-md-8 col-xs-12">
                                            <p class="font-r-normal text"><a class="" href="">0274-2920443</a> <br>
                                                Senin - Minggu <br> 24 Jam Non Stop</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="mar_top1 hidden-xs hidden-xxs"></div>
                                        <div class="mar_top2 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                        <div class="col-md-10 col-xs-12">
                                            <p class="font-r-normal text">Jl. Selokan Mataram Monjali<br>Karangjati MT I/304<br>RT019 RW 042<br>Sinduadi, Mlati, Sleman<br>55284</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 hidden640 footer-menu">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <div class="mar_top5 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                <p class="font-r-bold title">LAYANAN</p>
                                <div class="mar_top1"></div>
                                <ul class="list no-arrow font-r-normal text">
                                    <li><a href="">Domain</a></li>
                                    <li><a href="">Shared Hosting</a></li>
                                    <li><a href="">Cloud VPS Hosting</a></li>
                                    <li><a href="">Manage VPS Hosting</a></li>
                                    <li><a href="">Web Builder</a></li>
                                    <li><a href="">Keamanan SSL / HTTPS</a></li>
                                    <li><a href="">Jasa Pembuatan Website</a></li>
                                    <li><a href="">Program Afiliasi</a></li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 hidden640 footer-menu">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <div class="mar_top5 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                <p class="font-r-bold title">SERVICE HOSTING</p>
                                <div class="mar_top1"></div>
                                <ul class="list no-arrow font-r-normal text">
                                    <li><a href="">Hosting Murah</a></li>
                                    <li><a href="">Hosting Indonesia</a></li>
                                    <li><a href="">Hosting Singapore SG</a></li>
                                    <li><a href="">Hosting PHP</a></li>
                                    <li><a href="">Hosting WordPress</a></li>
                                    <li><a href="">Hosting Laravel</a></li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 hidden640 footer-tutorial">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <div class="mar_top5 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                <p class="font-r-bold title">TUTORIAL</p>
                                <div class="mar_top1"></div>
                                <ul class="list no-arrow font-r-normal text">
                                    <li><a href="">Knowledgebase</a></li>
                                    <li><a href="">Blog</a></li>
                                    <li><a href="">Cara Pembayaran</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 hidden640 info-callme">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <div class="mar_top5 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                <p class="font-r-bold title">TENTANG KAMI</p>
                                <div class="mar_top1"></div>
                                <ul class="list no-arrow font-r-normal text">
                                    <li><a href="">Tim Niagahoster</a></li>
                                    <li><a href="">Karir</a></li>
                                    <li><a href="">Events</a></li>
                                    <li><a href="">Penawaran &amp; Promo Spesial</a></li>
                                    <li><a href="">Kontak Kami</a></li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 hidden640 footer-menu">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <div class="mar_top5 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                <p class="font-r-bold title">KENAPA PILIH NIAGAHOSTER?</p>
                                <div class="mar_top1"></div>
                                <ul class="list no-arrow font-r-normal text">
                                    <li><a href="">Support Terbaik</a></li>
                                    <li><a href="">Garansi Harga Termurah</a></li>
                                    <li><a href="">Domain Gratis Selamanya</a></li>
                                    <li><a href="">Datacenter Hosting Terbaik</a></li>
                                    <li><a href="">Review Pelanggan</a></li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 hidden640 footer-menu">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <div class="mar_top5 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                <p class="font-r-bold title">NEWSLETTER</p>
                                <div class="mar_top1"></div>
                                <form method="get" action="" id="newsletter_submit" class="pull-left">
                                    <div class="input-group">
                                        <input class="form-control email_newsletter" name="email" id="samplees" value="" placeholder="Email" type="text">
                                        <span class="input-group-btn"><button class="btn btn-info btn2" type="submit">Berlangganan</button></span>
                                    </div>
                                </form>
                                <p class="mar_top1 font-r-light note">Dapatkan promo dan konten menarik dari penyedia <a href="" class="font-inherit">hosting terbaik</a> Anda.</p>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 hidden640 footer-tutorial">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <div class="mar_top5 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                <div class="row social-icon">
                                    <div class="col-lg-3 col-md-3 col-xxs-3 text-center">
                                        <a href="" target="_blank"><i class="zmdi zmdi-facebook zmdi-hc-lg zmdi-hc-border-circle-fb"></i></a>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-xxs-3 text-center">
                                        <a href="" target="_blank"><i class="zmdi zmdi-twitter zmdi-hc-lg zmdi-hc-border-circle-tw"></i></a>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-xxs-3 text-center">
                                        <a href="" target="_blank"><i class="fa fa-google-plus fa-lg fa-border"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 hidden640 hidden480 hidden-xss">
                                <div class="mar_top3 hidden-xs hidden-xxs"></div>
                                <div class="mar_top5 hidden-lg hidden-md hidden-sm show-xs show-xxs"></div>
                                <p class="font-r-bold title">PEMBAYARAN</p>
                                <ul class="logo-bank">

                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/logo-bca-bordered.svg"  alt="bank bca" class="bank-logo">
                                        </a>
                                    </li>

                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/mandiriclickpay.svg"  alt="mandiri clickpay" class="bank-logo">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/logo-bni-bordered.svg" alt="bank bri" class="bank-logo">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/visa.svg"  alt="visa" class="bank-logo">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/mastercard.svg"  alt="master card" class="bank-logo">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/atmbersama.svg"  alt="atm bersama" class="bank-logo">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/permatabank.svg" alt="bank permata" class="bank-logo">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/prima.svg"  alt="prima" class="bank-logo">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/images/alto.svg"  alt="alto" class="bank-logo">
                                        </a>
                                    </li>
                                </ul>
                                <p class="mar_top2 font-r-light note">Aktivasi instan dengan e-Payment. Hosting dan <a href="" class="font-inherit">domain</a> langsung aktif!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright_info">
                    <div class="container">
                        <div class="col-ms-12 col-sm-12">
                            <hr class="super-soft">
                        </div>
                        <div class="col-ms-9 col-sm-9 text-left">
                            Copyright ©2016 Niagahoster | Hosting powered by PHP7,
                            CloudLinux, CloudFlare, BitNinja and <a href="" class="font-inherit">DC Biznet Technovillage Jakarta</a><br>
                            Cloud <a href="" class="font-inherit">VPS Murah</a> powered by Webuzo
                            Softaculous, Intel SSD and cloud computing technology
                        </div>
                        <div class="col-ms-3 col-sm-3 font-white center-text">
                            <span class="syarat-dan-ketentuan"><a href="">Syarat dan Ketentuan</a> | <a href="">Kebijakan Privasi</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>